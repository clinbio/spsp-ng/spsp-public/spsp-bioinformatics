# SPSP Bioinformatics

## Description
Pointer to the repositories hosting the source code for the bioinformatics pipelines implemented on SPSP.

### SARS-CoV-2
We integrated in the SPSP Server the following tools:
- pangolin (updated twice per week)
- nextclade 1.11 (SARS-CoV-2 dataset updated twice per week)

### Influenza, RSV
Dedicated pipelines will be integrated into SPSP in the Autumn 2023 and Spring 2024. The source code will be publicly available once we start.

### Bacteria
We plan to implement the IMMense bacterial pipeline from the University of Zurich Applied Microbiology Resaerch group. To allow for collaborative work and contributions from others, this repo will be hosted on [GitHub SIB-SWISS](https://github.com/sib-swiss/SPSP-Bacteria-Bioinformatics)

## Contributing

We welcome contributions in the form of nextflow DSL2 subworkflows. 
Modules should be contributed on nf-core directly.

## License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.